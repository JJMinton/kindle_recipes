all:
	-pdflatex --output-directory=./output --shell-escape christmas_mince_pies.tex
	-pdflatex --output-directory=./output --shell-escape cookies.tex
	-pdflatex --output-directory=./output --shell-escape brownie.tex
	-pdflatex --output-directory=./output --shell-escape fruit_cake.tex
